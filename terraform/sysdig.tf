provider "sysdig" {
  sysdig_secure_url       = "https://app.us4.sysdig.com"
  sysdig_secure_api_token = "6639a793-ff66-4d9b-83a9-cd1ec7b95e5d"
}

provider "google" {
  project = "sinchan-roy-sysdig-interview"
  region = "europe-west3"
}

provider "google-beta" {
  project = "sinchan-roy-sysdig-interview"
  region = "europe-west3"
}

module "secure-for-cloud_example_single-project" {
  source = "sysdiglabs/secure-for-cloud/google//examples/single-project"
}
