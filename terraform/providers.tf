terraform {
  required_providers {
    sysdig = {
      source  = "sysdiglabs/sysdig"
    }
    google = {
      source  = "hashicorp/google"
      version = "4.30.0"
    }
  }
}