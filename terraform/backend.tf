terraform {
  backend "gcs" {
    bucket  = "sinchan-roy-sysdig-interview-terraform-states"
    prefix  = "terraform/state"
  }
}