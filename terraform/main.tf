resource "google_container_cluster" "primary" {
  name     = "mysysdig"
  location = "europe-west3"

  remove_default_node_pool = true
  initial_node_count       = 1

  project = "sinchan-roy-sysdig-interview"
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "mysysdig-pool"
  location   = "europe-west3"
  cluster    = google_container_cluster.primary.name
  node_count = 1

  project = "sinchan-roy-sysdig-interview"

  autoscaling {
    max_node_count = 3
    min_node_count = 1
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  upgrade_settings {
    max_surge       = 1
    max_unavailable = 0
  }

  node_config {
    preemptible  = true
    machine_type = "e2-standard-4"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    labels = {
      env      = "test"
      pii      = "false"
      purpose  = "interview"
      service  = "sysdig"
      team     = "sinchanroy"
    }
    image_type = "UBUNTU_CONTAINERD"
  }
}

resource "google_container_node_pool" "secondary_preemptible_nodes" {
  name       = "mysysdig-second-pool"
  location   = "europe-west3"
  cluster    = google_container_cluster.primary.name
  node_count = 1

  project = "sinchan-roy-sysdig-interview"

  autoscaling {
    max_node_count = 3
    min_node_count = 1
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  upgrade_settings {
    max_surge       = 1
    max_unavailable = 0
  }

  node_config {
    preemptible  = true
    machine_type = "e2-standard-4"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    labels = {
      env      = "test"
      pii      = "false"
      purpose  = "interview"
      service  = "sysdig"
      team     = "sinchanroy"
    }
    image_type = "UBUNTU_CONTAINERD"
  }
}
